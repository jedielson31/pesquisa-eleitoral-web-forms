﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using UpAds.PesquisaEleitoral.Data.Mapping;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Data
{
    public class PesquisaContext : DbContext
    {
        public PesquisaContext()
            :base("DefaultConnection")
        {
            Database.Log = t => Debug.Write(t);
        }

        public DbSet<Bairro> Bairros { get; set; }

        public DbSet<Candidato> Candidatos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new BairroMap());
            modelBuilder.Configurations.Add(new CandidatoMap());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {

            try
            {
                var data = ChangeTracker.Entries();

                foreach (var entity in data.Where(e => e.State == EntityState.Added))
                {
                    var type = entity.Entity.GetType();
                    var name = type.Name + "Id";
                    if ((Guid)entity.Property(name).CurrentValue == Guid.Empty)
                    {
                        entity.Property(name).CurrentValue = Guid.NewGuid();
                    }
                }

                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            return base.SaveChanges();
        }
    }
}
