﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Data.Mapping
{
    public class PesquisaUsuarioMap : EntityTypeConfiguration<PesquisaUsuario>
    {
        public PesquisaUsuarioMap()
        {
            HasKey(x => x.PesquisaUsuarioId);
            Property(x => x.PesquisaUsuarioId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(x => x.Candidato)
                .WithMany(x => x.Pesquisas)
                .HasForeignKey(x => x.CandidatoId);

            HasRequired(x => x.Bairro)
                .WithMany(x => x.Pesquisas)
                .HasForeignKey(x => x.BairroId);
        }
    }
}