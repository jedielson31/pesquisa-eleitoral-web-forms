﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Data.Mapping
{
    public class BairroMap : EntityTypeConfiguration<Bairro>
    {
        public BairroMap()
        {
            HasKey(x => x.BairroId);
            Property(x => x.BairroId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
