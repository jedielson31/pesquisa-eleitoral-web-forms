namespace UpAds.PesquisaEleitoral.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bairro",
                c => new
                    {
                        BairroId = c.Guid(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.BairroId);
            
            CreateTable(
                "dbo.Candidato",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Candidato");
            DropTable("dbo.Bairro");
        }
    }
}
