namespace UpAds.PesquisaEleitoral.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriacaoPEsquisa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PesquisaUsuario",
                c => new
                    {
                        PesquisaUsuarioId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                        BairroId = c.Guid(nullable: false),
                        CandidatoId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.PesquisaUsuarioId)
                .ForeignKey("dbo.Bairro", t => t.BairroId)
                .ForeignKey("dbo.Candidato", t => t.CandidatoId)
                .Index(t => t.BairroId)
                .Index(t => t.CandidatoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PesquisaUsuario", "CandidatoId", "dbo.Candidato");
            DropForeignKey("dbo.PesquisaUsuario", "BairroId", "dbo.Bairro");
            DropIndex("dbo.PesquisaUsuario", new[] { "CandidatoId" });
            DropIndex("dbo.PesquisaUsuario", new[] { "BairroId" });
            DropTable("dbo.PesquisaUsuario");
        }
    }
}
