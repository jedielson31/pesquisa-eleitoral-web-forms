using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<UpAds.PesquisaEleitoral.Data.PesquisaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UpAds.PesquisaEleitoral.Data.PesquisaContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Bairros.AddOrUpdate(
              p => p.Nome,
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Agua Verde"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Pinheirinho"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Jardim das Am�ricas"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Guabirotuba"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Cajuru"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Cap�o Raso"},
              new Bairro { BairroId = Guid.NewGuid(), Nome = "Agua Verde"}
            );
            //
        }
    }
}
