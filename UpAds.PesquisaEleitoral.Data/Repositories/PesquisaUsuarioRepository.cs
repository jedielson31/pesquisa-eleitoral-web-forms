﻿using System;
using System.Linq;
using UpAds.PesquisaEleitoral.Data.Repositories.Common;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;

namespace UpAds.PesquisaEleitoral.Data.Repositories
{
    public class PesquisaUsuarioRepository : RepositoryBase<PesquisaUsuario, Guid>, IPesquisaUsuarioRepository
    {
        public PesquisaUsuarioRepository(PesquisaContext context) : base(context)
        {
        }

        public PesquisaUsuario ObterPorUsuario(Guid usuarioId)
        {
            return DbSet.FirstOrDefault(x => x.UsuarioId == usuarioId);
        }
    }
}