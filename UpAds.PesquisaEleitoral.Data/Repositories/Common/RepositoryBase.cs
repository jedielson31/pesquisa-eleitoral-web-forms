﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Ddd.Services.Interfaces.Repositories;
using LinqKit;

namespace UpAds.PesquisaEleitoral.Data.Repositories.Common
{
    public abstract class RepositoryBase<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : class
    {
        protected PesquisaContext Context { get;}

        protected DbSet<TEntity> DbSet { get; }

        protected RepositoryBase(PesquisaContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public IEnumerable<TEntity> All(bool @readonly = false)
        {
            return DbSet;
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, bool @readonly = false)
        {
            var data =  DbSet.Where(predicate).AsExpandable();

            if (@readonly)
            {
                data.AsNoTracking();
            }

            return data;
        }

        public TEntity Get(TKey id)
        {
            return DbSet.Find(id);
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public void Save(TEntity entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            var entry = Context.Entry(entity);
            DbSet.Attach(entity);
            entry.State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                Context?.Dispose();
            }
        }
    }
}
