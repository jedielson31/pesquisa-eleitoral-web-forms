﻿using System;
using UpAds.PesquisaEleitoral.Data.Repositories.Common;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;

namespace UpAds.PesquisaEleitoral.Data.Repositories
{
    public class BairroRepository : RepositoryBase<Bairro, Guid>, IBairroRepository
    {
        public BairroRepository(PesquisaContext context) : base(context)
        {
        }
    }
}