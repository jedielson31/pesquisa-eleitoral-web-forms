﻿using System;
using UpAds.PesquisaEleitoral.Data.Repositories.Common;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;

namespace UpAds.PesquisaEleitoral.Data.Repositories
{
    public class CandidatoRepository : RepositoryBase<Candidato, Guid>, ICandidatoRepository
    {
        public CandidatoRepository(PesquisaContext context) : base(context)
        {
        }
    }
}