﻿using System;
using Ddd.Services.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories
{
    public interface IPesquisaUsuarioRepository : IRepository<PesquisaUsuario, Guid>
    {
        /// <summary>
        /// Obtém um usuário por Id
        /// </summary>
        /// <param name="usuarioId">O id do usuário</param>
        /// <returns>Um <see cref="PesquisaUsuario"/></returns>
        PesquisaUsuario ObterPorUsuario(Guid usuarioId);
    }
}