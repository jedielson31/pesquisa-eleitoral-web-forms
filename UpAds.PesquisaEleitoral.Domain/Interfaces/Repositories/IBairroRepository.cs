﻿using System;
using Ddd.Services.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories
{
    public interface IBairroRepository : IRepository<Bairro, Guid>
    {
    }
}
