﻿using System;
using System.Collections.Generic;
using Ddd.Services.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Domain.Interfaces.Services
{
    public interface ICandidatoService : IService<Candidato, Guid>, IDisposable
    {
        IEnumerable<Candidato> All(bool @readonly = false);
    }
}
