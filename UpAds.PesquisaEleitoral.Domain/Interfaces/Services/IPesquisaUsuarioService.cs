﻿using System;
using Ddd.Services.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Domain.Interfaces.Services
{
    public interface IPesquisaUsuarioService : IService<PesquisaUsuario, Guid>
    {
        bool JaParticipouDaPesquisa(Guid usuarioId);
    }
}