﻿using System;
using System.Collections.Generic;
using Ddd.Services.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Entities;

namespace UpAds.PesquisaEleitoral.Domain.Interfaces.Services
{
    public interface IBairroService : IService<Bairro, Guid>, IDisposable
    {
        IEnumerable<Bairro> All();
    }
}