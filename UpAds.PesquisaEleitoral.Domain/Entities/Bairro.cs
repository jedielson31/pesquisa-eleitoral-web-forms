﻿using System;
using System.Collections.Generic;
using Ddd.Validation.Common;
using Ddd.Validation.Extensions;
using Ddd.Validation.Interfaces;

namespace UpAds.PesquisaEleitoral.Domain.Entities
{
    public class Bairro
    {
        public Guid BairroId { get; set; }

        public string Nome  { get; set; }

        public ICollection<PesquisaUsuario> Pesquisas { get; set; }

        public IValidationResult Validate()
        {
            return new ValidationResult()
                .AssertArgumentNotNull(Nome, "Nome é obrigatório")
                .AssertArgumentNotEmpty(Nome, "Nome é obrigatório");
        }
    }
}