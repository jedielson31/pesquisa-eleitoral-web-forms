﻿using System;
using Ddd.Validation.Common;
using Ddd.Validation.Extensions;
using Ddd.Validation.Interfaces;

namespace UpAds.PesquisaEleitoral.Domain.Entities
{
    public class PesquisaUsuario : ISelfValidation
    {
        public Guid PesquisaUsuarioId { get; set; }

        public Guid UsuarioId { get; set; }

        public Bairro Bairro { get; set; }

        public Candidato Candidato { get; set; }

        public Guid BairroId { get; set; }

        public Guid CandidatoId { get; set; }

        public IValidationResult ValidationResult { get; private set; }

        public bool IsValid
        {
            get
            {
                ValidationResult = new ValidationResult();
                if (BairroId == Guid.Empty)
                {
                    ValidationResult.Add("O bairro é obrigatório");
                }

                if (CandidatoId == Guid.Empty)
                {
                    ValidationResult.Add("O candidato é obrigatório");
                }

                if (UsuarioId == Guid.Empty)
                {
                    ValidationResult.Add("O Usuario é obrigatório");
                }
                    
                return ValidationResult.IsValid;
            }
        }
    }
}