﻿using System;
using System.Collections.Generic;
using Ddd.Services.Common;
using Ddd.Services.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;

namespace UpAds.PesquisaEleitoral.Domain.Services
{
    public class CandidatoService : ServiceBase<Candidato, Guid>, ICandidatoService
    {
        private readonly ICandidatoRepository _repository;

        public CandidatoService(ICandidatoRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override IServiceOperationResult Create(Candidato entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entidade obrigatória");
            }

            var result = entity.Validate();

            if (!result.IsValid)
            {
                var retorno = new ServiceOperationResult();
                retorno.Add(result);
                return retorno;
            }

            return base.Create(entity);
        }

        public override IServiceOperationResult Update(Candidato entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), "Entidade obrigatória");
            }

            var result = entity.Validate();

            if (!result.IsValid)
            {
                var retorno = new ServiceOperationResult();
                retorno.Add(result);
                return retorno;
            }

            return base.Update(entity);
        }

        public IEnumerable<Candidato> All(bool @readonly = false)
        {
            return _repository.All(@readonly);
        }
    }
}