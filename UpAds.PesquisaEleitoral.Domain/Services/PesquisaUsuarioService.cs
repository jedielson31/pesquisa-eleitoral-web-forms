﻿using System;
using Ddd.Services.Common;
using Ddd.Services.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;

namespace UpAds.PesquisaEleitoral.Domain.Services
{
    public class PesquisaUsuarioService : ServiceBase<PesquisaUsuario, Guid>, IPesquisaUsuarioService
    {
        private readonly IPesquisaUsuarioRepository _repository;

        public PesquisaUsuarioService(IPesquisaUsuarioRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override IServiceOperationResult Create(PesquisaUsuario entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (!JaParticipouDaPesquisa(entity.UsuarioId))
            {
                return base.Create(entity);
            }

            var result = new ServiceOperationResult();
            result.Add("O usuário já participou da pesquisa");
            return result;
        }

        public override IServiceOperationResult Update(PesquisaUsuario entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return base.Update(entity);
        }

        public override IServiceOperationResult Delete(PesquisaUsuario entity)
        {
            throw new NotSupportedException();
        }

        public bool JaParticipouDaPesquisa(Guid usuarioId)
        {
            return _repository.ObterPorUsuario(usuarioId) != null;
        }
    }
}