﻿using System;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Candidato
{
    public partial class Create : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private ICandidatoRepository _repository;
        private ICandidatoService _service;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSalvar_OnClick(object sender, EventArgs e)
        {
            try
            {
                var bairro = CarregaObjeto();
                using (_context = new PesquisaContext())
                {
                    using (_repository = new CandidatoRepository(_context))
                    {
                        using (_service = new CandidatoService(_repository))
                        {
                            var result = _service.Create(bairro);
                            if (result.Success)
                            {
                                Response.Redirect("Index.aspx");
                            }

                            foreach (var resultError in result.Errors)
                            {
                                ModelState.AddModelError("", resultError);
                            }
                            ValidationSummaryBairro.ShowSummary = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                ValidationSummaryBairro.ShowSummary = true;
            }
        }

        private Domain.Entities.Candidato CarregaObjeto()
        {
            var candidato = new Domain.Entities.Candidato { Nome = TextBoxNome.Text };
            return candidato;
        }
    }
}