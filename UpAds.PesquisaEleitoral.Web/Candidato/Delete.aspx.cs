﻿using System;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Candidato
{
    public partial class Delete : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private ICandidatoRepository _repository;
        private ICandidatoService _service;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];
                CarregaFormulario(id);
            }
        }

        protected void ButtonDeletar_OnClick(object sender, EventArgs e)
        {
           using (_context = new PesquisaContext())
            {
                using (_repository = new CandidatoRepository(_context))
                {
                    using (_service = new CandidatoService(_repository))
                    {
                        var candidato = _service.Get(Guid.Parse(HiddenFieldId.Value));
                        var result = _service.Delete(candidato);

                        if (result.Success)
                        {
                            Response.Redirect("Index.aspx");
                        }
                    }
                }
            }
        }

        protected void ButtonCancelar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }

        private void CarregaFormulario(string id)
        {
            Guid objId;

            if (!Guid.TryParse(id, out objId))
            {
                throw new Exception("Não foi possível recuperar o ID do objeto");
            }

            Domain.Entities.Candidato candidato;

            using (_context = new PesquisaContext())
            {
                using (_repository = new CandidatoRepository(_context))
                {
                    using (_service = new CandidatoService(_repository))
                    {
                        candidato = _service.Get(objId);
                    }
                }
            }

            if (candidato == null)
            {
                throw new Exception("Não foi possível recuperar as informações do objeto");
            }

            LabelCandidato.Text = candidato.Nome;
            HiddenFieldId.Value = candidato.Id.ToString();
        }
    }
}