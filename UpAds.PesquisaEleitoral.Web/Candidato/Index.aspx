﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Candidato.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <asp:Button ID="ButtonNovo"
            runat="server"
            Text="Novo"
            CssClass="btn btn-primary"
            OnClick="ButtonNovo_OnClick" />
    </div>
    <asp:GridView ID="GridViewBairros"
        runat="server"
        CssClass="table table-hover table-striped"
        GridLines="None"
        ShowHeader="True"
        UseAccessibleHeader="True"
        AutoGenerateColumns="False"
        EmptyDataText="A consulta não retornou resultados"
        >
        <Columns>
            <asp:BoundField HeaderText="Nome" DataField="Nome" ReadOnly="True" />
            <asp:TemplateField ShowHeader="false">
                <ItemTemplate>
                    <asp:Button ID="ButtonEditarRow"
                        runat="server"
                        CommandName="Edit"
                        CommandArgument='<%# Eval("Id") %>'
                        Text="Editar"
                        CssClass="btn btn-primary"
                        OnClick="ButtonEditarRow_OnClick" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="false">
                <ItemTemplate>
                    <asp:Button ID="ButtonDeletarRow"
                        runat="server"
                        CommandName="Delete"
                        CommandArgument='<%# Eval("Id") %>'
                        Text="Deletar"
                        CssClass="btn btn-danger"
                        OnClick="ButtonDeletarRow_OnClick" />
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
</asp:Content>
