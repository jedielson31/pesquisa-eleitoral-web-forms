﻿using System;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Candidato
{
    public partial class Edit : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private ICandidatoRepository _repository;
        private ICandidatoService _service;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];
                CarregaFormulario(id);
            }
        }

        protected void ButtonSalvar_OnClick(object sender, EventArgs e)
        {
            try
            {
                var bairro = CarregaObjeto();
                using (_context = new PesquisaContext())
                {
                    using (_repository = new CandidatoRepository(_context))
                    {
                        using (_service = new CandidatoService(_repository))
                        {
                            var result = _service.Update(bairro);
                            if (result.Success)
                            {
                                Response.Redirect("Index.aspx");
                            }

                            foreach (var resultError in result.Errors)
                            {
                                ModelState.AddModelError("", resultError);
                            }
                            ValidationSummaryBairro.ShowSummary = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                ValidationSummaryBairro.ShowSummary = true;
            }
        }

        private void CarregaFormulario(string id)
        {
            Guid objId;

            if (!Guid.TryParse(id, out objId))
            {
                throw new Exception("Não foi possível recuperar o ID do objeto");
            }

            Domain.Entities.Candidato candidato;

            using (_context = new PesquisaContext())
            {
                using (_repository = new CandidatoRepository(_context))
                {
                    using (_service = new CandidatoService(_repository))
                    {
                        candidato = _service.Get(objId);
                    }
                }
            }

            if (candidato == null)
            {
                throw new Exception("Não foi possível recuperar as informações do objeto");
            }

            TextBoxNome.Text = candidato.Nome;
            HiddenFieldId.Value = candidato.Id.ToString();
        }

        private Domain.Entities.Candidato CarregaObjeto()
        {
            var bairro = new Domain.Entities.Candidato()
            {
                Nome = TextBoxNome.Text,
                Id = Guid.Parse(HiddenFieldId.Value)
            };
            return bairro;
        }
    }
}