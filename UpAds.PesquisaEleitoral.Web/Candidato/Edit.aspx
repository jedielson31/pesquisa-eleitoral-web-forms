﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Candidato.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h2>Alterar Bairro</h2>
    <hr />
    <asp:ValidationSummary ID="ValidationSummaryBairro" 
        CssClass="text-danger"
        runat="server" />

    <asp:HiddenField runat="server" ID="HiddenFieldId"/>
        
    <div class="form-group">
        <asp:Label ID="LabelNome" AssociatedControlID="TextBoxNome" Text="Nome" runat="server"></asp:Label>
        <asp:TextBox 
            ID="TextBoxNome" 
            runat="server"
            CssClass="form-control"
            >
        </asp:TextBox>
        <asp:RequiredFieldValidator
            ID="RequiredFieldValidatorNome"
            runat="server"
            ErrorMessage="O nome é obrigatório"
            ControlToValidate="TextBoxNome"
            CssClass="text-danger"></asp:RequiredFieldValidator>
    </div>
    <div class="row">

        <asp:Button runat="server"
            ID="ButtonSalvar"
            Text="Salvar"
            CssClass="btn btn-primary"
            OnClick="ButtonSalvar_OnClick" />
    </div>

</asp:Content>
