﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Report1.ascx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Reports.UserControls.Report1" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" %>
<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%"
    ShowToolBar="False"
    ZoomMode="FullPage"
    AsyncRendering="False"
    SizeToReportContent="True">
    <LocalReport ReportPath="Reports\Report1.rdlc">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSetReport1" />
        </DataSources>
    </LocalReport>
</rsweb:ReportViewer>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="UpAds.PesquisaEleitoral.Web.Reports.DataSetReportsTableAdapters.Report1TableAdapter"></asp:ObjectDataSource>

