﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VotosPorBairro.ascx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Reports.UserControls.VotosPorBairro" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
    ShowToolBar="true"
    ZoomMode="FullPage"
    AsyncRendering="False"
    SizeToReportContent="True">
    <LocalReport ReportPath="Reports\Report2.rdlc">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSetTotais" />
        </DataSources>
    </LocalReport>
    
</rsweb:ReportViewer>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="UpAds.PesquisaEleitoral.Web.Reports.DataSetReportsTableAdapters.Report2TableAdapter"></asp:ObjectDataSource>

