﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UpAds.PesquisaEleitoral.Web.Startup))]
namespace UpAds.PesquisaEleitoral.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
