﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioVotosPorCandidato.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Pesquisa.RelatorioVotosPorCandidato" %>

<%@ Register Src="~/Reports/UserControls/Report1.ascx" TagPrefix="uc1" TagName="Report1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <h2>Relatório de Votos por Candidato</h2>

    <uc1:Report1 runat="server" ID="Report1" />
</asp:Content>
