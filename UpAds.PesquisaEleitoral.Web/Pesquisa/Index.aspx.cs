﻿using System;
using System.Linq;
using Ddd.Services.Interfaces.Services;
using Microsoft.AspNet.Identity;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Entities;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Pesquisa
{
    public partial class Index : System.Web.UI.Page
    {
        private IBairroService _bairroService;
        private ICandidatoService _candidatoService;
        private IBairroRepository _bairroRepository;
        private ICandidatoRepository _candidatoRepository;
        private PesquisaContext _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CarregaDropDowns();
                VerificaParticipacao();
            }
        }

        private void VerificaParticipacao()
        {
            using (_context = new PesquisaContext())
            {
                using (var repo = new PesquisaUsuarioRepository(_context))
                {
                    var antiga = repo.ObterPorUsuario(Guid.Parse(User.Identity.GetUserId()));

                    if (antiga != null)
                    {
                        PreparaBotaoOperacao(true);
                        HiddenFieldId.Value = antiga.PesquisaUsuarioId.ToString();
                        return;
                    }

                    PreparaBotaoOperacao(false);
                }
            }
        }

        private void PreparaBotaoOperacao(bool editar)
        {
            ButtonSalvar.Visible = !editar;
            ButtonEditar.Visible = editar;

            LabelAviso.Visible = editar;
        }

        private void CarregaDropDowns()
        {
            using (_context = new PesquisaContext())
            {
                _candidatoRepository = new CandidatoRepository(_context);
                _bairroRepository = new BairroRepository(_context);

                DropDownListBairro.DataSource = _bairroRepository.All(true).ToList();
                DropDownListBairro.DataTextField = "Nome";
                DropDownListBairro.DataValueField = "BairroId";
                DropDownListBairro.DataBind();

                DropDownListCandidato.DataSource = _candidatoRepository.All(true).ToList();
                DropDownListCandidato.DataTextField = "Nome";
                DropDownListCandidato.DataValueField = "Id";
                DropDownListCandidato.DataBind();

                _candidatoRepository.Dispose();
                _bairroRepository.Dispose();
            }
        }

        private void Salvar(bool editar)
        {
            try
            {
                using (_context = new PesquisaContext())
                {
                    _candidatoRepository = new CandidatoRepository(_context);
                    _bairroRepository = new BairroRepository(_context);

                    var pesquisa = CarregaObjeto(_candidatoRepository, _bairroRepository);

                    if (!ModelState.IsValid)
                    {
                        return;
                    }

                    using (var repo = new PesquisaUsuarioRepository(_context))
                    {
                        using (var service = new PesquisaUsuarioService(repo))
                        {
                            var result = editar ? service.Update(pesquisa) : service.Create(pesquisa);

                            if (!result.Success)
                            {
                                foreach (var resultError in result.Errors)
                                {
                                    ModelState.AddModelError("", resultError);
                                }
                                return;
                            }
                            Response.Redirect("~/Default.aspx");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
        }

        protected void ButtonSalvar_OnClick(object sender, EventArgs e)
        {
            Salvar(false);
        }

        protected void ButtonEditar_OnClick(object sender, EventArgs e)
        {
            Salvar(true);
        }

        private PesquisaUsuario CarregaObjeto(ICandidatoRepository candidatoRepository, IBairroRepository bairroRepository)
        {
            Guid bairroId;
            Guid candidatoId;

            if (!Guid.TryParse(DropDownListBairro.SelectedValue, out bairroId))
            {
                ModelState.AddModelError("", "O bairro obrigatório");
            }

            if (!Guid.TryParse(DropDownListCandidato.SelectedValue, out candidatoId))
            {
                ModelState.AddModelError("", "O candidato é obrigatório");
            }

            if (!ModelState.IsValid)
            {
                return null;
            }

            Guid pesquisaId;

            Guid.TryParse(HiddenFieldId.Value, out pesquisaId);

            return new PesquisaUsuario
            {
                BairroId = bairroId,
                //Bairro = bairroRepository.Get(bairroId),
                CandidatoId = candidatoId,
                //Candidato = candidatoRepository.Get(candidatoId),
                UsuarioId = Guid.Parse(User.Identity.GetUserId()),
                PesquisaUsuarioId = pesquisaId
            };

        }

        protected void ButtonCancelar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}