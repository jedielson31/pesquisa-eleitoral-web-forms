﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioVotosPorIdade.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Pesquisa.RelatorioVotosPorIdade" %>

<%@ Register Src="~/Reports/UserControls/VotosPorIdade.ascx" TagPrefix="uc1" TagName="VotosPorIdade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:VotosPorIdade runat="server" ID="VotosPorIdade" />
</asp:Content>
