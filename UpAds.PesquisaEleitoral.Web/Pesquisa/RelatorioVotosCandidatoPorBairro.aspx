﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioVotosCandidatoPorBairro.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Pesquisa.RelatorioVotosCandidatoPorBairro" %>

<%@ Register Src="~/Reports/UserControls/VotosPorBairro.ascx" TagPrefix="uc1" TagName="VotosPorBairro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:VotosPorBairro runat="server" id="VotosPorBairro" />
</asp:Content>
