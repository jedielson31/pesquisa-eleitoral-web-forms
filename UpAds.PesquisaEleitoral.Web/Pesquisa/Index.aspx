﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Pesquisa.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Página de Pesquisa</h1>
    
    <asp:Label ID="LabelAviso" runat="server" Text="Você já participou da votação. Caso responda novamente, seu voto será alterado."
        CssClass="text-danger"></asp:Label>

    <div class="container">
        <asp:ValidationSummary ID="ValidationSummaryErros" runat="server" CssClass="text-danger" />
        
        <asp:HiddenField ID="HiddenFieldId" runat="server"/>

        <div class="row">
            <div class="col-md-4">
                <asp:Label runat="server" ID="LabelBairro" AssociatedControlID="DropDownListBairro" Text="Bairro"></asp:Label>
                <div class="col-md-12">
                    <asp:DropDownList ID="DropDownListBairro" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <asp:Label runat="server" ID="LabelCandidato" AssociatedControlID="DropDownListCandidato" Text="Bairro"></asp:Label>
                <div class="col-md-12">
                    <asp:DropDownList ID="DropDownListCandidato" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 10px">
            <div class="col-md-8">
                <asp:Button runat="server"
                    ID="ButtonSalvar"
                    CssClass="btn btn-primary"
                    Text="Responder"
                    OnClick="ButtonSalvar_OnClick" />
                
                <asp:Button runat="server"
                    ID="ButtonEditar"
                    CssClass="btn btn-primary"
                    Text="Responder"
                    OnClick="ButtonEditar_OnClick" />

                <asp:Button runat="server"
                    ID="ButtonCancelar"
                    CssClass="btn btn-danger"
                    Text="Cancelar"
                    OnClick="ButtonCancelar_OnClick" />
            </div>
        </div>
    </div>
</asp:Content>
