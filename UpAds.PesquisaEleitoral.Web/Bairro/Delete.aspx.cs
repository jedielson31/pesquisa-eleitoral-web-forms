﻿using System;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Bairro
{
    public partial class Delete : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private IBairroRepository _repository;
        private IBairroService _service;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];
                CarregaFormulario(id);
            }
        }

        private void CarregaFormulario(string id)
        {
            Guid objId;

            if (!Guid.TryParse(id, out objId))
            {
                throw new Exception("Não foi possível recuperar o ID do objeto");
            }

            Domain.Entities.Bairro bairro;

            using (_context = new PesquisaContext())
            {
                using (_repository = new BairroRepository(_context))
                {
                    using (_service = new BairroService(_repository))
                    {
                        bairro = _service.Get(objId);
                    }
                }
            }

            if (bairro == null)
            {
                throw new Exception("Não foi possível recuperar as informações do objeto");
            }

            LabelBairro.Text = bairro.Nome;
            HiddenFieldId.Value = bairro.BairroId.ToString();
        }

        protected void ButtonDeletar_OnClick(object sender, EventArgs e)
        {
            var bairro = new Domain.Entities.Bairro()
            {
                Nome = LabelBairro.Text,
                BairroId = Guid.Parse(HiddenFieldId.Value)
            };

            using (_context = new PesquisaContext())
            {
                using (_repository = new BairroRepository(_context))
                {
                    using (_service = new BairroService(_repository))
                    {
                        var result = _service.Delete(bairro);

                        if (result.Success)
                        {
                            Response.Redirect("Index.aspx");
                        }
                    }
                }
            }
        }

        protected void ButtonCancelar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}