﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Bairro
{
    public partial class Edit : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private IBairroRepository _repository;
        private IBairroService _service;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Request.QueryString["id"];
                CarregaFormulario(id);
            }
        }

        private void CarregaFormulario(string id)
        {
            Guid objId;

            if (!Guid.TryParse(id, out objId))
            {
                throw new Exception("Não foi possível recuperar o ID do objeto");
            }

            Domain.Entities.Bairro bairro;

            using (_context = new PesquisaContext())
            {
                using (_repository = new BairroRepository(_context))
                {
                    using (_service = new BairroService(_repository))
                    {
                        bairro = _service.Get(objId);
                    }
                }
            }

            if (bairro == null)
            {
                throw new Exception("Não foi possível recuperar as informações do objeto");
            }

            TextBoxNome.Text = bairro.Nome;
            HiddenFieldId.Value = bairro.BairroId.ToString();
        }

        protected void ButtonSalvar_OnClick(object sender, EventArgs e)
        {
            try
            {
                var bairro = CarregaObjeto();
                using (_context = new PesquisaContext())
                {
                    using (_repository = new BairroRepository(_context))
                    {
                        using (_service = new BairroService(_repository))
                        {
                            var result = _service.Update(bairro);
                            if (result.Success)
                            {
                                Response.Redirect("Index.aspx");
                            }

                            foreach (var resultError in result.Errors)
                            {
                                ModelState.AddModelError("", resultError);
                            }
                            ValidationSummaryBairro.ShowSummary = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                ValidationSummaryBairro.ShowSummary = true;
            }
        }

        private Domain.Entities.Bairro CarregaObjeto()
        {
            var bairro = new Domain.Entities.Bairro
            {
                Nome = TextBoxNome.Text,
                BairroId = Guid.Parse(HiddenFieldId.Value)
            };
            return bairro;
        }
    }
}