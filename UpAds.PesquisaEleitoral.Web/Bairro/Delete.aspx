﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="UpAds.PesquisaEleitoral.Web.Bairro.Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Deletar Bairro</h2>
    <hr/>
    
    <h4>Tem certeza que deseja deletar o bairro <asp:Label runat="server" ID="LabelBairro"></asp:Label></h4>
    

    <div class="form-group">
        <asp:HiddenField runat="server" 
            ID="HiddenFieldId"
            />
        <asp:Button 
            runat="server"
            ID="ButtonDeletar"
            CssClass="btn btn-danger"
            Text="Confirmar"
            OnClick="ButtonDeletar_OnClick"
            />
        <asp:Button 
            runat="server"
            ID="ButtonCancelar"
            CssClass="btn btn-success"
            Text="Cancelar"
            OnClick="ButtonCancelar_OnClick"
            />
    </div>
</asp:Content>
