﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using UpAds.PesquisaEleitoral.Data;
using UpAds.PesquisaEleitoral.Data.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Repositories;
using UpAds.PesquisaEleitoral.Domain.Interfaces.Services;
using UpAds.PesquisaEleitoral.Domain.Services;

namespace UpAds.PesquisaEleitoral.Web.Bairro
{
    public partial class Index : System.Web.UI.Page
    {
        private PesquisaContext _context;
        private IBairroRepository _repository;
        private IBairroService _service;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregaGrid();
            }
        }

        private void CarregaGrid()
        {
            GridViewBairros.DataSource = GetGridItens();
            GridViewBairros.DataBind();
        }

        private IEnumerable<Domain.Entities.Bairro> GetGridItens()
        {
            List<Domain.Entities.Bairro> retorno;

            using (_context = new PesquisaContext())
            {
                using (_repository = new BairroRepository(_context))
                {
                    using (_service = new BairroService(_repository))
                    {
                        retorno = _service.All().ToList();
                    }
                }
            }

            return retorno;
        }

        protected void ButtonNovo_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Create.aspx");
        }

        protected void ButtonEditarRow_OnClick(object sender, EventArgs e)
        {
            //Get the button that raised the event
            var button = (Button)sender;
            Response.Redirect("Edit.aspx?id=" + button.CommandArgument);

        }

        protected void ButtonDeletarRow_OnClick(object sender, EventArgs e)
        {
            var button = (Button)sender;
            Response.Redirect("Delete.aspx?id=" + button.CommandArgument);
        }
    }
}