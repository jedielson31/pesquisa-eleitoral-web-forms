using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using UpAds.PesquisaEleitoral.Web.Models;

namespace UpAds.PesquisaEleitoral.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "UpAds.PesquisaEleitoral.Web.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var hasher = new PasswordHasher();
            var userId = Guid.NewGuid().ToString();
            var roleId = Guid.NewGuid().ToString();

            var user = new ApplicationUser
            {
                Id = userId,
                UserName = "admin@pesquisa.com.br",
                Email = "admin@pesquisa.com.br",
                PasswordHash = hasher.HashPassword("@dminP4ss"),
                SecurityStamp = Guid.NewGuid().ToString()
            };

            context.Users.AddOrUpdate(u => u.UserName,
                user
            );
            

            context.Roles.AddOrUpdate(r => r.Name,
                new IdentityRole
                {
                    Id = roleId,
                    Name = "Administrator",
                    Users = {
                        new IdentityUserRole
                        {
                            UserId = userId,
                            RoleId = roleId
                        }
                    },
                }
            );
        }
    }
}
