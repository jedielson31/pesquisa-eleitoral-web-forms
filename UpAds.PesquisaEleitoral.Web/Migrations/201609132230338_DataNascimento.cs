namespace UpAds.PesquisaEleitoral.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataNascimento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "DataDeNascimento", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DataDeNascimento");
        }
    }
}
